package com.lpq.takeaway.dto;

import com.lpq.takeaway.pojo.Dish;
import com.lpq.takeaway.pojo.DishFlavor;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

/**
 * 数据传输对象 用来封装页面传输过来的复杂json数据
 * 扩展了Dish
 */
@Data
public class DishDto extends Dish {

    //菜品对应的口味数据
    private List<DishFlavor> flavors = new ArrayList<>();

    private String categoryName;

    private Integer copies;
}

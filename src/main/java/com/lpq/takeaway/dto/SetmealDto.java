package com.lpq.takeaway.dto;
import com.lpq.takeaway.pojo.Setmeal;
import com.lpq.takeaway.pojo.SetmealDish;
import lombok.Data;
import java.util.List;

/**
 * 数据传输对象 用来封装页面传输过来的复杂json数据
 * 扩展了Setmeal
 */
@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;

    private String categoryName;
}

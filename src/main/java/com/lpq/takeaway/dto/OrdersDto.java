package com.lpq.takeaway.dto;

import com.lpq.takeaway.pojo.OrderDetail;
import com.lpq.takeaway.pojo.Orders;
import lombok.Data;
import java.util.List;

/**
 * 数据传输对象 用来封装页面传输过来的复杂json数据
 * 扩展了Orders
 */
@Data
public class OrdersDto extends Orders {

    private String userName;

    private String phone;

    private String address;

    private String consignee;

    private List<OrderDetail> orderDetails;

    //每个订单中菜品种类的数量
    private int sumNum;
	
}

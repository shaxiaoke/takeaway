package com.lpq.takeaway.common;

/**
 * 基于ThreadLocal封装的工具类 用于保存和获取当前登录用户的id
 * @author: PQ丶Lee
 * @createTime: 2023-06-30 10:10
 **/
@SuppressWarnings({"All"})
public class BaseContext {
    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    public static void setCurrentId(Long id){
        threadLocal.set(id);
    }

    public static Long getCurrentId(){
        return threadLocal.get();
    }
}

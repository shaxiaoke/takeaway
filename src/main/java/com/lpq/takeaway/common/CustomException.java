package com.lpq.takeaway.common;

/**
 * 自定义业务异常类
 * @author: PQ丶Lee
 * @createTime: 2023-06-30 20:11
 **/
public class CustomException extends RuntimeException{
    public CustomException(String message){
        super(message);
    }
}

package com.lpq.takeaway.filter;

import com.alibaba.fastjson.JSON;
import com.lpq.takeaway.common.BaseContext;
import com.lpq.takeaway.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登录过滤器 防止未登录用户直接访问
 * @author: PQ丶Lee
 * @createTime: 2023-06-29 08:10
 **/
@Slf4j
@WebFilter(filterName = "LoginCheckFilter",urlPatterns = "/*")
public class LoginCheckFilter implements Filter {
    //路径匹配器 支持通配符
    public static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        //1.获取本次请求的URI
        String requestURI = request.getRequestURI();
        log.info("拦截到请求：{}",requestURI);
        //定义不需要处理的请求路径
        String[] urls = new String[]{
                "/employee/login",
                "/employee/logout",
                "/backend/**",
                "/front/**",
                "/common/**",
                "/user/sendMsg", //移动端发送短信
                "/user/login", //移动端登录
                // swagger相关的路径
                "/doc.html",
                "/webjars/**",
                "/swagger-resources",
                "/v2/api-docs"
        };
        //2.判断本次请求是否被处理
        boolean check = check(urls,requestURI);
        //3.如果不需要被处理则放行
        if (check){
            log.info("本次请求不需要被处理：{}",requestURI);
            filterChain.doFilter(request,response);
            return;
        }
        //4.1 判断服务端登录状态 如果已登录则直接放行
        if (request.getSession().getAttribute("employee") != null){
            log.info("用户已登录,用户的ID：{}",request.getSession().getAttribute("employee"));
            //取出用户id
            Long empId = (Long) request.getSession().getAttribute("employee");
            //利用封装的工具类 存入对应线程中
            BaseContext.setCurrentId(empId);
            filterChain.doFilter(request,response);
            return;
        }
        //4.2 判断移动端用户登录状态 如果已登录则直接放行
        if (request.getSession().getAttribute("user") != null){
            log.info("用户已登录,用户的ID：{}",request.getSession().getAttribute("user"));
            //取出用户id
            Long userId = (Long) request.getSession().getAttribute("user");
            //利用封装的工具类 存入对应线程中
            BaseContext.setCurrentId(userId);
            filterChain.doFilter(request,response);
            return;
        }
        log.info("用户未登录");
        //5.如果未登录则返回未登录状态 通过输出流方式向客户端页面响应数据
        response.getWriter().write(JSON.toJSONString(R.error("NOTLOGIN")));
    }
    /**
     * 路径匹配 检查本次请求是否放行
     * @param urls
     * @param requestURI
     * @return
     */
    public boolean check(String[] urls,String requestURI){
        for (String url : urls) {
            //第一个参数是url 第二个参数是传过来的requestURI
            boolean match = PATH_MATCHER.match(url, requestURI);
            if (match) {
                return true;
            }
        }
        return false;
    }

}

package com.lpq.takeaway.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lpq.takeaway.pojo.SetmealDish;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {
}

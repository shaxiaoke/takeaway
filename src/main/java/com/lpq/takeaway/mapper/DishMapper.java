package com.lpq.takeaway.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lpq.takeaway.pojo.Dish;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DishMapper extends BaseMapper<Dish> {
}

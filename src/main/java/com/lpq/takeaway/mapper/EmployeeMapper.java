package com.lpq.takeaway.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lpq.takeaway.pojo.Employee;
import org.apache.ibatis.annotations.Mapper;

@Mapper //标识为mapper 交给spring管理
public interface EmployeeMapper extends BaseMapper<Employee> {
}

package com.lpq.takeaway.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lpq.takeaway.pojo.Orders;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrdersMapper extends BaseMapper<Orders> {
}

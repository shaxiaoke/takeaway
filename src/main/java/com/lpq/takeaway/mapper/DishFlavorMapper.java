package com.lpq.takeaway.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lpq.takeaway.pojo.DishFlavor;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DishFlavorMapper extends BaseMapper<DishFlavor> {
}

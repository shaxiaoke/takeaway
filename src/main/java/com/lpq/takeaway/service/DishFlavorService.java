package com.lpq.takeaway.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lpq.takeaway.pojo.DishFlavor;

public interface DishFlavorService extends IService<DishFlavor> {
}

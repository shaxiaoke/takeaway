package com.lpq.takeaway.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lpq.takeaway.pojo.Category;

public interface CategoryService extends IService<Category> {

    public void remove(Long ids);
}

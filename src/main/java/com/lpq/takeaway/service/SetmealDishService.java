package com.lpq.takeaway.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lpq.takeaway.pojo.SetmealDish;

public interface SetmealDishService extends IService<SetmealDish> {
}

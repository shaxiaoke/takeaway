package com.lpq.takeaway.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lpq.takeaway.pojo.OrderDetail;


public interface OrderDetailService extends IService<OrderDetail> {
}

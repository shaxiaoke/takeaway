package com.lpq.takeaway.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lpq.takeaway.pojo.User;

public interface UserService extends IService<User> {
}

package com.lpq.takeaway.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lpq.takeaway.mapper.EmployeeMapper;
import com.lpq.takeaway.pojo.Employee;
import com.lpq.takeaway.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author: PQ丶Lee
 * @createTime: 2023-06-28 18:30
 **/
@Service //标识为service 交给spring管理
@Slf4j
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {
}

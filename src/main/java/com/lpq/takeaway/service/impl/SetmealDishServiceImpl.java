package com.lpq.takeaway.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lpq.takeaway.mapper.SetmealDishMapper;
import com.lpq.takeaway.pojo.SetmealDish;
import com.lpq.takeaway.service.SetmealDishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author: PQ丶Lee
 * @createTime: 2023-07-04 10:27
 **/
@Slf4j
@Service
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper,SetmealDish> implements SetmealDishService {
}

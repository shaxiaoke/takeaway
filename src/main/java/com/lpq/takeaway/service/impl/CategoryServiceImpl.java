package com.lpq.takeaway.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lpq.takeaway.common.CustomException;
import com.lpq.takeaway.mapper.CategoryMapper;
import com.lpq.takeaway.pojo.Category;
import com.lpq.takeaway.pojo.Dish;
import com.lpq.takeaway.pojo.Setmeal;
import com.lpq.takeaway.service.CategoryService;
import com.lpq.takeaway.service.DishService;
import com.lpq.takeaway.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: PQ丶Lee
 * @createTime: 2023-06-30 15:55
 **/
@Service
@Slf4j
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {
    @Autowired
    private SetmealService setmealService;
    @Autowired
    private DishService dishService;
    /**
     * 根据id删除分类，删除之前要先判断看是否与其它套餐或菜品有关联
     * @param ids
     */
    @Override
    public void remove(Long ids) {
        LambdaQueryWrapper<Dish> dishQueryWrapper = new LambdaQueryWrapper<>();
        //添加查询条件 根据菜品分类id进行查询
        dishQueryWrapper.eq(Dish::getCategoryId,ids);
        int dish_count = dishService.count(dishQueryWrapper);
        //查询当前菜品分类是否关联了菜品，如果关联，则不能删除 要抛出一个业务异常
        if (dish_count > 0){ //如果关联数大于0
            //抛出自定义的业务异常
            throw new CustomException("当前分类项关联了菜品，无法删除");
        }

        LambdaQueryWrapper<Setmeal> setmealQueryWrapper = new LambdaQueryWrapper<>();
        //添加查询条件 根据套餐分类id进行查询
        setmealQueryWrapper.eq(Setmeal::getCategoryId,ids);
        int setmeal_count = setmealService.count(setmealQueryWrapper);
        //查询当前套餐分类是否关联了菜品，如果关联，则不能删除 要抛出一个业务异常
        if (setmeal_count > 0){ //如果关联数大于0
            //抛出自定义的业务异常
            throw new CustomException("当前分类项关联了套餐，无法删除");
        }

        //如果上面两个if都没有处理 则表示要删除的分类无关联 直接删除
        super.removeById(ids);
    }
}

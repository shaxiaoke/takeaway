package com.lpq.takeaway.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lpq.takeaway.mapper.AddressBookMapper;
import com.lpq.takeaway.pojo.AddressBook;
import com.lpq.takeaway.service.AddressBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author: PQ丶Lee
 * @createTime: 2023-07-05 09:20
 **/
@Service
@Slf4j
public class AddressBookServiceImpl extends ServiceImpl<AddressBookMapper, AddressBook> implements AddressBookService {
}

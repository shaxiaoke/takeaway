package com.lpq.takeaway.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lpq.takeaway.mapper.UserMapper;
import com.lpq.takeaway.pojo.User;
import com.lpq.takeaway.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author: PQ丶Lee
 * @createTime: 2023-07-04 19:57
 **/
@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}

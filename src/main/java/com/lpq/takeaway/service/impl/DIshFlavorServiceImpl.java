package com.lpq.takeaway.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lpq.takeaway.mapper.DishFlavorMapper;
import com.lpq.takeaway.pojo.DishFlavor;
import com.lpq.takeaway.service.DishFlavorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author: PQ丶Lee
 * @createTime: 2023-07-02 09:31
 **/
@Service
@Slf4j
public class DIshFlavorServiceImpl extends ServiceImpl<DishFlavorMapper, DishFlavor> implements DishFlavorService {
}

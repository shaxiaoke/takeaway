package com.lpq.takeaway.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lpq.takeaway.pojo.AddressBook;

public interface AddressBookService extends IService<AddressBook> {
}

package com.lpq.takeaway.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lpq.takeaway.pojo.Employee;

public interface EmployeeService extends IService<Employee> {
}

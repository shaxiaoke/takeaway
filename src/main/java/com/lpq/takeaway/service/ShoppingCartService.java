package com.lpq.takeaway.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lpq.takeaway.pojo.ShoppingCart;

public interface ShoppingCartService extends IService<ShoppingCart> {
}

package com.lpq.takeaway.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lpq.takeaway.common.BaseContext;
import com.lpq.takeaway.common.R;
import com.lpq.takeaway.pojo.ShoppingCart;
import com.lpq.takeaway.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author: PQ丶Lee
 * @createTime: 2023-07-05 17:15
 **/
@Slf4j
@SuppressWarnings({"all"})
@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {
    @Autowired
    private ShoppingCartService shoppingCartService;

    /**
     * 添加到购物车
     * @param shoppingCart
     * @return
     */
    @PostMapping("/add")
    public R<ShoppingCart> add(@RequestBody ShoppingCart shoppingCart){
        log.info("购物车的数据->{}",shoppingCart);
        //设置当前用户的id 指定是哪个用户的购物车数据
        Long currentId = BaseContext.getCurrentId();
        shoppingCart.setUserId(currentId);

        //获取菜品id
        Long dishId = shoppingCart.getDishId();
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,currentId);

        if (dishId != null){
            //表示添加到购物车的是菜品
            queryWrapper.eq(ShoppingCart::getDishId,dishId);
        }else {
            //表示添加到购物车的是套餐
            queryWrapper.eq(ShoppingCart::getSetmealId,shoppingCart.getSetmealId());
        }

        //查询当前菜品是否已经在购物车中
        ShoppingCart one = shoppingCartService.getOne(queryWrapper);

        if (one != null){
            //如果已经存在 则数量加一
            Integer number = one.getNumber();
            one.setNumber( ++number );
            //执行更新操作
            shoppingCartService.updateById(one);
        } else {
            //如果不存在 则添加到购物车 数量初始为1
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCartService.save(shoppingCart);
            one = shoppingCart;
        }
        return R.success(one);
    }

    /**
     * 减少购物车菜品/套餐的数量
     * @param shoppingCart
     * @return
     */
    @PostMapping("/sub")
    public R<ShoppingCart> sub(@RequestBody ShoppingCart shoppingCart){
        // 当前用户id
        Long userId = BaseContext.getCurrentId();
        // 当前购物车中的菜品id
        Long dishId = shoppingCart.getDishId();

        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,userId);

        if(dishId != null){ //添加到购物车的是菜品
            queryWrapper.eq(ShoppingCart::getDishId,dishId);
        }else{ //添加到购物车的是套餐
            queryWrapper.eq(ShoppingCart::getSetmealId,shoppingCart.getSetmealId());
        }
        //SQL : select * from shopping_cart where user_id = ? and dish_id/setmeal_id = ?
        //获得当前购物车
        ShoppingCart cartServiceOne = shoppingCartService.getOne(queryWrapper);

        if (cartServiceOne != null) {
            //获得当前购物车中该菜品数量
            Integer number = cartServiceOne.getNumber();

            log.info("当前菜品数量 = {}",number);
            //如果数量大于 0 才能 - 1
            if (number > 0 ) {
                cartServiceOne.setNumber(number - 1);
                shoppingCartService.updateById(cartServiceOne);

                //定义一个swap暂存number 因为不知道为什么老是会溢出
                Integer swap = number;
                if (--swap == 0) { //如果此时菜品数量为0了，则移出购物车 即直接删除
                    shoppingCartService.removeById(cartServiceOne.getId());
                }
            } else{ //否则直接删除
                shoppingCartService.removeById(cartServiceOne.getId());
            }
        }

        return R.success(cartServiceOne);
    }

    /**
     * 查看购物车
     * @return
     */
    @GetMapping("/list")
    public R<List<ShoppingCart>> list(){
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,BaseContext.getCurrentId());
        queryWrapper.orderByAsc(ShoppingCart::getCreateTime);
        List<ShoppingCart> list = shoppingCartService.list(queryWrapper);
        return R.success(list);
    }

    /**
     * 清空购物车
     * @return
     */
    @DeleteMapping("/clean")
    public R<String> clean(){
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,BaseContext.getCurrentId());

        shoppingCartService.remove(queryWrapper);
        return R.success("清空购物车成功");
    }

}
